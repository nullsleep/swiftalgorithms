//
//  Delegation.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/16/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import UIKit

// Reference: Understanding Delegates and Delegation in Swift 4 - https://www.appcoda.com/swift-delegate/

// Apple Documentation about Delegation:
// Delegation is a simple and powerful pattern in which one object in a program acts on behalf of, or in coordination with, another object. The delegating object keeps a reference to the other object–the delegate–and at the appropriate time sends a message to it. The message informs the delegate of an event that the delegating object is about to handle or has just handled. The delegate may respond to the message by updating the appearance or state of itself or other objects in the application, and in some cases it can return a value that affects how an impending event is handled. The main value of delegation is that it allows you to easily customize the behavior of several objects in one central object.

// A delegate is an object that acts on behalf of, or in coordination with, another object when that object encounters an event in a program. The delegating object is often a responder object–that is, an object inheriting from NSResponder in AppKit or UIResponder in UIKit–that is responding to a user event. The delegate is an object that is delegated control of the user interface for that event, or is at least asked to interpret the event in an application-specific manner.

// The programming mechanism of delegation gives objects a chance to coordinate their appearance and state with changes occurring elsewhere in a program, changes usually brought about by user actions. More importantly, delegation makes it possible for one object to alter the behavior of another object without the need to inherit from it. The delegate is almost always one of your custom objects, and by definition it incorporates application-specific logic that the generic and delegating object cannot possibly know itself.

// Requirement: a protocol
// Delegation is a design pattern that enables a class or structure to hand off (or delegate) some of its responsibilities to an instance of another type. This design pattern is implemented by defining a protocol that encapsulates the delegated responsibilities, such that a conforming type (known as a delegate) is guaranteed to provide the functionality that has been delegated. Delegation can be used to respond to a particular action, or to retrieve data from an external source without needing to know the underlying type of that source.
// A protocol defines a blueprint of methods, properties, and other requirements that suit a particular task or piece of functionality. The protocol can then be adopted by a class, structure, or enumeration to provide an actual implementation of those requirements. Any type that satisfies the requirements of a protocol is said to conform to that protocol.


// MARK: - LogoDownloaderDelegate

protocol LogoDownloaderDelegate {
    // Classes that adopt this protocol must define this method, and hpefully do something in that definition.
    func didFinishDownloading(_ sender: LogoDownloader)
}


// MARK: - LogoDownloader class implementation. This is the delegating class. An instance of this class is a delegating object.
class LogoDownloader {
    
    var logoURL: String
    var image: UIImage?
    
    // weak var delegate: LogoDownloaderDelegate?
    var delegate: LogoDownloaderDelegate?
    
    init(logoURL: String) {
        self.logoURL = logoURL
    }
    
    func downloadLogo() {
        // Start the image download task asyncronously by submitting it to the default background queue; this task is submitted and
        // DispatchQueue.global returns immediately,¡.
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let imageURL = URL(string: self.logoURL)
            
            // For simplicity PUPOSEFULLY downloading the image using a synchronous call (NSData), but doing it in the background.
            let imageData = NSData(contentsOf: imageURL!)
            self.image = UIImage(data: imageData! as Data)
            print("Image Downloaded")
            
            // Once the image finishes downloading, jump onto the main thread to update the UI
            DispatchQueue.main.async {
                self.didDownloadImage()
            }
        }
    }
    
    // Since this class has a reference to the delegate, "at the appropiate time it sends a message to" the delegate. Finishing
    // the logo download is definitely the appropiate time.
    func didDownloadImage() {
        delegate?.didFinishDownloading(self)
    }
}
