//
//  ViewController.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/11/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import UIKit

// Declaring that ViewController is the delegate for UIScrollViewDelegate, LogoDownloaderDelegate
class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var logoDownloader: LogoDownloader?
    
    // MARK: ViewDidLoad life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Executes an specific algorithm
        executeAlgorithm()
        
        // Adding a circle using the Drawing class
        addACircleToTheView()
       
        // The main screen implements a login screen that only appears (fades in) when the background image is downloaded and the delegate has responded.
        setLoginViews()
    }
}

// MARK: - ViewController UI elements

private extension ViewController {
    
    func addACircleToTheView() {
        self.view.addSubview(Drawing.drawCircle())
    }
    
    func setLoginViews() {
        scrollView.delegate = self // Extra scroll delegate
        
        imageView.alpha = 0.0
        loginView.alpha = 0.0
        
        // Background image
        let imageURL: String = "https://i.pinimg.com/564x/1d/0c/6f/1d0c6f64b3d19cbde3f058f8e7d2b013.jpg"
        
        // Construct a LogoDownloader to download the NASA file.
        logoDownloader = LogoDownloader(logoURL: imageURL)
        logoDownloader?.delegate = self
        logoDownloader?.downloadLogo()
        
        if logoDownloader?.delegate == nil {
            loginView.alpha = 1.0
        }
    }
}

// MARK: - Drawing LogoDownloaderDelegate

extension ViewController: LogoDownloaderDelegate {
    
    func didFinishDownloading(_ sender: LogoDownloader) {
        imageView.image = logoDownloader?.image
        
        // Animate the appearance of this ViewController's user interface
        UIView.animate(withDuration: 2.0, delay: 0.5, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.loadingLabel.alpha = 0.0
            self.imageView.alpha = 1.0
        }) { (completed:Bool) in
            if completed {
                UIView.animate(withDuration: 2.0) {
                    self.loginView.alpha = 1.0
                }
            }
        }
    }
}

// MARK: - Algorithms implementations

private extension ViewController {

    func executeAlgorithm() {
        //MergeSort.execute()
        //MostCommonElement.execute()
        //ReversedLinkedList.execute()
        //FizzBuzz.execute()
        //BinarySearch.execute()
        //MinimalTree.execute()
        //Factorials.execute()
        //ReverseEveryOtherWord.execute()
        //FibonacciSequence.execute()
        //WrappingAnArray.execute()
        //HigherOrderFunctions.execute()
        //SearchBinaryTree.execute()
        //RetainCycle.execute()
    }
}
