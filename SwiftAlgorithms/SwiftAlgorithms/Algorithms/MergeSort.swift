//
//  MergeSort.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/11/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=DfO089qWEE8

class MergeSort {
    
    static func execute() {
        var numbers: [Int] = []
        for _ in 0..<50 {
            let randomInt = Int(arc4random_uniform(UInt32(1000)))
            numbers.append(randomInt)
        }
        print("Unsorted Array:", numbers, "\n")
        print("Sorted Array:", mergeSort(unsortedArray: numbers))
    }
    
    // Split the Arrays
    static func mergeSort(unsortedArray: [Int]) -> [Int] {
        guard unsortedArray.count > 1 else {
            return unsortedArray
        }
        
        let leftArray = Array(unsortedArray[0..<unsortedArray.count/2])
        let rightArray = Array(unsortedArray[unsortedArray.count/2..<unsortedArray.count])
        
        return merge(left: mergeSort(unsortedArray: leftArray), right: mergeSort(unsortedArray: rightArray))
    }
    
    // Merge Arrays
    static func merge(left: [Int], right: [Int]) -> [Int] {
        var mergedArray: [Int] = []
        var left = left
        var right = right
        
        while left.count > 0 && right.count > 0 {
            if left.first! < right.first! {
                mergedArray.append(left.removeFirst())
            } else {
                mergedArray.append(right.removeFirst())
            }
        }
        
        return mergedArray + left + right
    }
}
