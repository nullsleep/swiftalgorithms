//
//  FizzBuzz.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/12/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=yxORFL_UipQ

// Numbers in sequential order. If a number is divisible by 3 you print fizz,
// if its divisible by 5 you print buzz and if it is both divisible by 3 and 5
// you print fizzBuzz

class FizzBuzz {
    
    //static let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    static var numbers = [Int]()
    
    static func execute() {
        // Filling up the array of numbers
        for i in 1...1000 {
            numbers.append(i)
        }
        
        // Playing FizzBuzz
        for num in numbers {
            if num % 15 == 0 {
                print("\(num) fizz buzz")
            } else if num % 3 == 0 {
                print("\(num) fizz")
            } else if num % 5 == 0 {
                print("\(num) buzz")
            } else {
                print(num)
            }
        }
    }
}
