//
//  SwiftHigherOrderFunctions.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=fbYMaJciMZY

// Higher Order Functions: Filter, Map, Reduce

class HigherOrderFunctions {
    static let numbers = [1, 2, 3, 4, 3, 3]
    
    static func execute() {
        print("filter")
        print(filterArray(nums: numbers, numberToFilterBy: 3))
        print("--------")
        print(simplifiedFilterArray(nums: numbers, numberToFilterBy: 3))
        print(evenNumberFilterArray(nums: numbers))
        print("\n-------- --------\n")
        transform()
        print("--------")
        simplifiedTransform()
        print("\n-------- --------\n")
        reduce()
        print("--------")
        reduceSimplified()
    }
    
    // 1. Filter into [3, 3, 3], then filter into [2, 4] (even numbers)
    static func filterArray(nums: [Int], numberToFilterBy: Int) -> [Int] {
        var filteredArray = [Int]()
        for num in nums {
            if num == numberToFilterBy {
                filteredArray.append(num)
            }
        }
        return filteredArray
    }
    
    static func simplifiedFilterArray(nums: [Int], numberToFilterBy: Int) -> [Int] {
        let filteredArray = nums.filter({return $0 == 3})
        return filteredArray
    }
    
    static func evenNumberFilterArray(nums: [Int]) -> [Int] {
        let filteredArray = nums.filter({return $0 % 2 == 0})
        return filteredArray
    }
    
    // 2. transform [1, 2, 3, 4] -> [2, 4, 6, 8] using map
    static func transform() {
        var transformedArray = [Int]()
        for number in [1, 2, 3, 4] {
            transformedArray.append(number * 2)
        }
        print(transformedArray)
    }
    
    static func simplifiedTransform() {
        let transformed = [1, 2, 3, 4].map({return $0 * 2})
        print(transformed)
    }
    
    // 3. sum [1, 2, 3, 4] -> 10 using reduce
     static func reduce() {
        var sumElements = 0
        for number in [1, 2, 3, 4] {
            sumElements += number
        }
        print(sumElements)
    }
    
    static func reduceSimplified() {
        let sum = [1, 2, 3, 4].reduce(0, {sum, number in sum + number})
        print(sum)
    }
    
}
