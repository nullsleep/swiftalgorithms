//
//  Factorials.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference https://www.youtube.com/watch?v=EManXGSm_ak

// Factorials: 5! = 5 * 4 * 3 * 2 * 1
// 3! = 3 * 2 * 1
// 0! = 1

class Factorials {

    static func execute() {
        print(linearFactorialOf(value: 5))
        print(linearFactorialOf(value: 3))
        print(linearFactorialOf(value: 0))
        print("---")
        print(recursiveFactorialOf(value: 5))
        print(recursiveFactorialOf(value: 3))
        print(recursiveFactorialOf(value: 0))
    }
    
    // UInt = non negative int
    static func linearFactorialOf(value: UInt) -> UInt {
        
        // Terminating condition
        if value == 0 {
            return 1
        }
        
        var product: UInt = 1
        for i in 1...value {
            product = product * i
        }
        
        return product
    }
    
    static func recursiveFactorialOf(value: UInt) -> UInt {
        
        // Terminating condition
        if value == 0 {
            return 1
        }
        
        return value * recursiveFactorialOf(value: value - 1)
    }
}
