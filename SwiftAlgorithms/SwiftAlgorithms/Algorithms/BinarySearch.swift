//
//  BinarySearch.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/12/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Given a sorted array of integers, search through it and return true or false if you can find a given number

// Reference https://www.youtube.com/watch?v=CkYDljd_7M8

class BinarySearch {
    
//    static let numbers = [1, 2, 3, 5, 7, 23, 64, 77, 121, 143, 156, 213, 216, 566]
    static var numbers = [Int]()
    
    static func execute() {
        for i in 1...100 {
            numbers.append(i)
        }
//        print("Linear search result:", linearSearch(searchValue: 1, array: numbers))
//        print("Binary search result:", binarySearch(searchValue: 64, array: numbers))
//        print("Binary search result:", binarySearch(searchValue: 4, array: numbers))
//        print("Binary search result:", binarySearch(searchValue: 156, array: numbers))
//        print("Binary search result:", binarySearch(searchValue: 2, array: numbers))
        print("Binary search result:", binarySearch(searchValue: 99, array: numbers))
    }
    
    
    // Linear solution O(N)
    static func linearSearch(searchValue: Int, array: [Int]) -> Bool {
        for num in array {
            if num == searchValue {
                return true
            }
        }
        return false
    }
    
    // Logaritmic solution Olog(N)
    static func binarySearch(searchValue: Int, array: [Int]) -> Bool {
        // Divide the array and search
        var leftIndex = 0
        var rightIndex = array.count - 1
        
        while leftIndex <= rightIndex {
            let middleIndex = (leftIndex + rightIndex) / 2
            let middleValue = array[middleIndex]
            
            print("MiddleValue: \(middleValue), leftIndex\(leftIndex), rightIndex\(rightIndex), [\(array[leftIndex]), \(array[rightIndex])]")
            
            if middleValue == searchValue {
                return true
            }
            
            if searchValue < middleValue {
                rightIndex = middleIndex - 1
            }
            
            if searchValue > middleValue {
                leftIndex = middleIndex + 1
            }
        }
        
        return false
    }
}
