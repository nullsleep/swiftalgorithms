//
//  Closures.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/15/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://medium.com/@abhimuralidharan/functional-swift-all-about-closures-310bc8af31dd

// - Closures are self-contained blocks of functionality that can be passed around and used in your code.
//  "Closures and functions are the same with different syntax"
// - First class objects, so they can be nested and passed around.

class Closures {
    
    // Basic closures example
    func execute() {
        let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
        
        var reversedNames = names.sorted(by: backward)
        
        reversedNames = names.sorted(by: { (s1: String, s2: String) -> Bool in
            return s1 > s2
        })
        
        print(reversedNames)
    }
    
    func backward(_ s1: String, _ s2: String) -> Bool {
        return s1 > s2
    }
    
    // Functions
    func Functions() {
        
        // Function type of doSomething is (Int, Int) -> (Int, Int)
        func doSomething(param1: Int, param2: Int) -> (returnParam1: Int, returnParam2: Int) {
            return (param1, param2)
        }
        let returnTuple = doSomething(param1: 1, param2: 2)
        print(returnTuple.0)
        print(returnTuple.1)
        
        // Fucntion types can be used as a return type form nested functions or as parameters
        func personInTheHouse() -> ((String) -> String) {
            func doProcess(process: String) -> (String) {
                return "The person is \(process)"
            }
            
            return doProcess
        }
        
        let person = personInTheHouse()
        print(person("playing tenis"))
    }
    
    // MARK: - CLOSURES
    // Unnamed expressions written in a lightweight syntax that can capture values from their surrounding context.
    // The parameters in a closure expression syntax can be in-out parameters, but they can't have a default values.
    // { (params) -> returnType in
    //      Statements
    // }
    func Closures() {
        
        // Simple function example.
        func addTwoNumbers(number1: Int, number2: Int) -> Int {
            return number1 + number2
        }
        print(addTwoNumbers(number1: 8, number2: 2))
        
        // MARK: - Closure equivalent of the function above
        let addClosure: (Int, Int) -> Int = { (number1, number2) in
            return number1 + number2
        }
        print(addClosure(8, 2))
        
        // Shorthand Argument Names version of the closure above
        let shortHandAddClosure: (Int, Int) -> Int = {
            return $0 + $1
        }
        print(shortHandAddClosure(8, 2))
        
        // Simplified version of the above closure.
        // * If there are multiple lines of code inside the closure body, we can't omit the return statement.
        let shorterHandAddClosure: (Int, Int) -> Int = { $0 + $1 }
        print(shorterHandAddClosure(8, 2))
        
        // Inferred Type Closure
        let inferredClosure = { (x: Int, y: Int) -> Int in x + y }
        print(inferredClosure(1, 99))
        
        // Return types can also be inferred:
        // Function type: (Int) -> (Int)
        let inferredReturnTypeClosure = { (number: Int) in number * number }
        print(inferredReturnTypeClosure(8))
        
        // MARK: - Closure that takes nothing and returns a string.
        // A closure can be of type () -> (SomeType)
        let callStringWithClosure: () -> String = { () in
            return "Hello"
        }
        print(callStringWithClosure())
        
        // Since, we don't care about the input parameters here, we can omit the '() in' inside the
        let simplifiedCallStringClosure: () -> String = { return "Hello" }
        print(simplifiedCallStringClosure)
        
        // Since it returns a String and it doesn't take any parameters, we can omit the type as well.
        let simplerCallStringClosure = { "Hello, closure here too" }
        print(simplerCallStringClosure)
        
        // Closures and functions are fist class types in Swift and can be treated like a normal value. You can:
        // - Assign a function/closure to a local variable.
        // - Pass a function/closure as an argument.
        // - Return a function/closure.
        
        // MARK: - Mehtods with completion callback using closure.
        var shoppingList = ["key": "value"]
        
        // The folowing code has a function which has three parameters. One is a dictionary, the other two are closures which will act
        // as callback functions when the process is done
        func callSomeMethodWithParams(_ params: [AnyHashable: Any],
                                      onSuccess success: @escaping (_ JSON: Any) -> Void,
                                      onFailure failure: @escaping (_ error: Error?, _ params: [AnyHashable: Any]) -> Void) {
            print("\n" + String(describing: params))
            
            let error: Error? = NSError(domain:"", code:1, userInfo: nil)
            var responseArray: [Any]?
            responseArray = [1, 2, 3, 4, 5]
            
            if let responseArr = responseArray {
                success(responseArr)
            }
            if let err = error {
                failure(err, params)
            }
        }
        
        callSomeMethodWithParams(shoppingList, onSuccess: { (JSON) in
            print("\nSuccess. Response recieved...: " + String(describing: JSON))
        }) { (error, params) in
            if let err = error {
                print("\nError: " + err.localizedDescription)
            }
            print("\nParameters passed are: " + String(describing: params))
        }
        
        // MARK: - Returning a closure from a function
        var addNumsClosure:(Int, Int) -> Int = { $0 + $1 }
        func returnClosure() -> (Int, Int) -> Int {
            return addNumsClosure
        }
        print(returnClosure()(10, 20))
        
        let returnedClosure = returnClosure() // Returns a closure of type (Int, Int) -> Int
        print(returnedClosure(20, 10))
        
    }
}
