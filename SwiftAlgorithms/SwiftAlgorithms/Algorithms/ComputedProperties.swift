//
//  ComputedProperties.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 10/9/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

/* Computed Properties
    - Pieces of code that will calculate themselves everytime you call them.
    - Always variables, never constants
    - Can be used in or outside classes, structs, enums
    - Since their value can change each time they are called, they can also change any other value in the enclosing scope if a setter is used.
*/
import Foundation

class ComputedProperties {
    var x = 10
    var y = 10
    
    var sumProperty: Int {
        get { // This get can be removed
            return x + y
        }
    }
    
    func execute() {
        print(sumProperty)
    }
}
