//
//  RetainCycle.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/14/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference

class RetainCycle {
    
    static func execute() {
        //let bob = Person(name: "Bob")
        
        // This the only we can make bob nil, by making him an optional
        var bob: Person? = Person(name: "Bob")
        print("Person \(bob?.name ?? "nn") created")
        
        // Setting the person to nil calls the deinit function
        //bob = nil
        
        var apt: Apartment? = Apartment(number: 123)
        
        // MARK: THE PROBLEM
        // This causes the retain cycle, is kept in memory and there is no way of retaining it.
        // Apartment is holding a reference to person
        apt?.tenant = bob
        // Bob is holding a reference to apartment
        bob?.apartment = apt
        
        // This doesn't corrently deinitialize both varaibles.
        bob = nil
        apt = nil
    }
    
    static func testingARC() {
        let objeto = Objeto()           // ARC object reference count = 1
        let reference2 = objeto         // ARC object reference count = 2
        let reference3 = reference2     // ARC object reference count = 3
        let reference4 = objeto         // ARC object reference count = 2
        
        print("\(objeto.name) - \(reference2.name) - \(reference3.name) - \(reference4.name)")
        
        // Once the references to object are removed ARC will automatically deallocate object for you.
//        let object = null        //ARC object reference count: 3
//        var reference2 = null    //ARC object reference count: 2
//        var reference3 = null    //ARC object reference count: 1
//        var reference4 = null    //ARC object reference count: 0
    }
}

class Person {
    let name: String
    var apartment: Apartment?
    
    init(name: String) {
        self.name = name
    }
    
    deinit {
        print("\(name) is being deinitialized")
    }
}

class Apartment {
    let number: Int
    //var tenant: Person?
    // MARK: - SOLUTION: Since the reference is not strong, the reference count for the Person tenant is not increased. This is recommended by the documentation.
    weak var tenant: Person?
    
    init(number: Int) {
        self.number = number
    }
    
    deinit {
        print("Apartment \(number) is being deinitialized")
    }
}

class Objeto {
    var name: String = ""
}
