//
//  binaryTree.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=p8x6nGzwwhc

// Minimal Tree: Given a sorted (increasing order) array, with unique integer elements,
// write an algorithm to create a binary search tree with minimal height.

class MinimalTree {
    static var array = [1, 5, 15,  50, 55, 65, 75]
    
    static func execute() {
        let tree = binaryTree(array, 0, array.count - 1)
        print(tree?.value ?? "No tree value")
        print(tree?.leftChild?.value ?? "No left child value")
        print(tree?.rightChild?.value ?? "No left child value")
    }
    
    static func binaryTree(_ array: [Int], _ firstIndex: Int, _ lastIndex: Int) -> Nodo? {
        if lastIndex < firstIndex {
            return nil
        }
        
        let middleIndex = (lastIndex - firstIndex) / 2 + firstIndex
        let nodo = Nodo(val: array[middleIndex])
        nodo.leftChild = binaryTree(array, firstIndex, middleIndex - 1)
        nodo.rightChild = binaryTree(array, middleIndex + 1, lastIndex)
        
        return nodo
    }
}

class Nodo {
    var value: Int
    var leftChild: Nodo?
    var rightChild: Nodo?
    
    init(val: Int) {
        self.value = val
    }
}
