//
//  MostCommonElement.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/11/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=xif4OkW2iQQ

// Find the most common element in an Array

class MostCommonElement {
    
    static var colorArray = ["red", "green", "black", "blue", "yellow", "yellow", "yellow", "yellow", "black", "black", "blue", "green", "green", "green", "green", "green", "green"]
    
    static func execute() {
        print(getMostCommonColor(array: colorArray))
    }
    
    static func getMostCommonColor(array: [String]) -> String {
        
        var colorCountDictionary = [String: Int]()
        
        for color in array {
            if let count = colorCountDictionary[color] {
                colorCountDictionary[color] = count + 1
            } else {
                colorCountDictionary[color] = 1
            }
        }
        
        var mostCommonName = ""
        
        for key in colorCountDictionary.keys {
            if mostCommonName == "" {
                mostCommonName = key
            } else {
                let count = colorCountDictionary[key]!
                if count > colorCountDictionary[mostCommonName]! {
                    mostCommonName = key
                }
            }
            print("\(key): \(colorCountDictionary[key]!)")
        }
        
        return mostCommonName
    }
}
