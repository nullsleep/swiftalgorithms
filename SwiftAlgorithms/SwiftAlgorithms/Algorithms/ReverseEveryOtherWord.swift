//
//  ReverseEveryOtherWord.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=4sv0QmJZ1OY

// 1. Reverse every other word in sentence
// 2. Remove all of the bowels from the reversed words

class ReverseEveryOtherWord {
    
    static let sampleSentence = "Lets start today by completing a very interesting challenge"
    
    static func execute() {
        print(reverseWordsInSentence(sentence: sampleSentence))
    }
    
    // Part 1
    static func reverseWordsInSentence(sentence: String) -> String {
        let allWords = sentence.components(separatedBy: " ")
        var newSentence = ""
        
        for index in 0...allWords.count - 1 {
            let word = allWords[index]
            
            if newSentence != "" {
                newSentence += " "
            }
            
            // odd numbers: num % 2 == 1
            if index % 2 == 1 {
                let reverseWord = String(word.reversed())
                // Part 1
                //newSentence += reverseWord
                
                // Part 2
                newSentence += reverseWord.stringByRemovingVowels()
            } else {
                newSentence += word
            }
            
        }
        return newSentence
    }
}

// Part 2
extension String {
    func stringByRemovingVowels() -> String {
        var newWord = self
        for vowel in ["a", "e", "i", "o", "u"] {
            newWord = newWord.replacingOccurrences(of: vowel, with: "")
        }
        return newWord
    }
}
