//
//  SearchBinaryTree.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=beQEimO3-3E

// Implement a search algorithm that searches through this tree for a particular seachValue and returns true if it finds it.
//            10
//          /    \
//         5      14
//        /      /  \
//       1      11   20

class SearchBinaryTree {
    
    static func execute() {
        // Right branch
        let twentyNode = Node2(value: 20, leftChild: nil, rightChild: nil)
        let elevenNode = Node2(value: 11, leftChild: nil, rightChild: nil)
        let fourteenNode = Node2(value: 14, leftChild: elevenNode, rightChild: twentyNode)
        
        // Left branch
        let oneNode = Node2(value: 1, leftChild: nil, rightChild: nil)
        let fiveNode = Node2(value: 5, leftChild: oneNode, rightChild: nil)
        
        let tenRootNode = Node2(value: 10, leftChild: fiveNode, rightChild: fourteenNode)
        
        print(searchBinaryTree(node: tenRootNode, searchValue: 10))
        print(searchBinaryTree(node: tenRootNode, searchValue: 5))
        print(searchBinaryTree(node: tenRootNode, searchValue: 0))
        print(searchBinaryTree(node: tenRootNode, searchValue: 14))
        print(searchBinaryTree(node: tenRootNode, searchValue: 20))
    }
    
    static func searchBinaryTree(node: Node2?, searchValue: Int) -> Bool {
        if node == nil {
            return false
        }
        
        if node?.value == searchValue {
            return true
        } else if searchValue < node!.value {
            return searchBinaryTree(node: node?.leftChild, searchValue: searchValue)
        } else {
            return searchBinaryTree(node: node?.rightChild, searchValue: searchValue)
        }
    }
    
    // Using an array. This lopps more times than a recursive call and it is more inefficient.
    func searchTree(searchValue: Int) -> Bool {
        // Same representation of the three above
        let list = [1, 5, 10, 11, 14, 20]
        let index = list.index(where: {$0 == searchValue})
        return index! >= 0
    }
}

class Node2 {
    let value: Int
    var leftChild: Node2?
    var rightChild: Node2?
    
    init(value: Int, leftChild: Node2?, rightChild: Node2?) {
        self.value = value
        self.leftChild = leftChild
        self.rightChild = rightChild
    }
}
