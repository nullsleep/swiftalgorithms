//
//  WrappingAnArray.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=h5Edr4J18Wk

class WrappingAnArray {
    // We have a list of tracks
    static let tracks = ["a", "b", "c", "d", "e"]
    
    // The algorithm should return this playlist if we select track "d"
    // ["d", "e", "a", "b", "c"]
    
    static func execute() {
        print(wrappingLinear(tracks: tracks, selectedTrack: "d"))
        print(wrappingLinear(tracks: tracks, selectedTrack: "b"))
        print("-----")
        print(wrappingSimplier(tracks: tracks, selectedTrack: "d"))
        print(wrappingSimplier(tracks: tracks, selectedTrack: "b"))
    }
    
    static func wrappingLinear(tracks: [String], selectedTrack: String) -> [String] {
        var playlist = [String]()
        var priorTracks = [String]()
        
        for track in tracks {
            if track == selectedTrack || playlist.count > 0 {
                playlist.append(track)
            } else {
                priorTracks.append(track)
            }
        }
        
        return playlist + priorTracks
    }
    
    static func wrappingSimplier(tracks: [String], selectedTrack: String) -> [String] {
        
        // $0 represents the item that is being iterated over inside tracks
        let index = tracks.index(where: {return $0 == selectedTrack})
        
        // let prefixArray = tracks.prefix(upTo: 3) // ["a", "b", "c"]
        let prefixArray = tracks.prefix(upTo: index!)
        //let sufixArray = tracks.suffix(from: 3) // ["d", "e"]
        let sufixArray = tracks.suffix(from: index!)
        let res = sufixArray + prefixArray
        
        return Array(res)
    }
}
