//
//  ReverseLinkedList.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/12/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=gNu-F_LnC0I

// What is a linked list?
// 1->2->3->nil

class ReversedLinkedList {
    static func execute() {
        let threeNode = Node(value: 3, next: nil)
        let twoNodes = Node(value: 2, next: threeNode)
        let oneNode = Node(value: 1, next: twoNodes)
        
        printList(head: oneNode)
        
        // Should be 3, 2, 1
        let myReversedList = reverseList(head: oneNode)
        printList(head: myReversedList)
    }
    
    static func printList(head: Node?) {
        print("Printing out a list of nodes")
        
        var currentNode = head
        while currentNode != nil {
            print(currentNode?.value ?? -1)
            currentNode = currentNode?.next
        }
    }
    
    static func reverseList(head: Node?) -> Node? {
        var currentNode = head
        var prev: Node?
        var next: Node?
        
        // Each cicle flips the arrow:
        // 1->2->3->nil
        // nil<-2->3->nil
        // nil<-1<-2->3->nil
        // nil<-1<-2<-3 = 3->2->1->nil
        
        while currentNode != nil {
            next = currentNode?.next
            currentNode?.next = prev
            
            print("Previous: \(prev?.value ?? -1)", "Current: \(currentNode?.value ?? -1)", "Next: \(next?.value ?? -1)")
            
            prev = currentNode
            currentNode = next
        }
        
        return prev
    }
}

// Define a data structure that supports this list
class Node {
    let value: Int
    var next: Node?
    
    init(value: Int, next: Node?) {
        self.value = value
        self.next = next
    }
}
