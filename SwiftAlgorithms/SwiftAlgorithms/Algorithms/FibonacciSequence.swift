//
//  FibonacciSequence.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/13/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// Reference: https://www.youtube.com/watch?v=Es4Tk0nU1OQ

// Fibonacci: the first two numbers in the Fibonacci sequence are either 1 and 1, or 0 and 1,
// depending on the chosen starting point of the sequence, and each subsequent number is
// the sum of the previous two.
// The sequence Fn of Fibonacci numbers is defined byt the recurrence raltion:
// Fn = Fn-1 + Fn-2
// With seed values: F1 = 1, F2 = 2 or F0 = 0, F1 = 1

class FibonacciSequence {
    
    static func execute() {
        print(FibFor(numSteps: 6))
        print(FibFor(numSteps: 1))
        print("----")
        print([0, 1] + FibRecursionFor(numSteps: 1, first: 0, second: 1))
        print([0, 1] + FibRecursionFor(numSteps: 6, first: 0, second: 1))
        print([0, 1] + FibRecursionFor(numSteps: 9, first: 0, second: 1))
    }
    
    static func FibFor(numSteps: Int) -> [Int] {
        var sequence = [0, 1]
        
        if numSteps <= 1 {
            return sequence
        }
        
        for _ in 0...numSteps - 2 {
            let first = sequence[sequence.count - 2]
            let second = sequence.last!
            sequence.append(first + second)
        }
        
        return sequence
    }
    
    // first and second are seed values
    static func FibRecursionFor(numSteps: Int, first: Int, second: Int) -> [Int] {
        if numSteps == 0 {
            return []
        }
        
        print("Values to append", first, second)
        
        return [first + second] + FibRecursionFor(numSteps: numSteps - 1, first: second, second: first + second)
    }
}
