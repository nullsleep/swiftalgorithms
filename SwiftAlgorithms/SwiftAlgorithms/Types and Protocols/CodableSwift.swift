//
//  Codable.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 10/12/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation

// In a nutshell, Encoding is the process of transforming your own custom type, class or struct to external data representation
// type like JSON or plist or something else & Decoding is the process of transforming external data representation type like JSON
// or plist to your own custom type, class or struct.

class CodableSwift {
    
    // MARK: Swift 4 new Standard library now includes new Protocols:
    // Econdable: For Encoding model to external representation of data like JSON or plist
    // Decodable: For decoding the external representation of data to your model.
    // Codable: For both encoding and decoding. Apple defines it as: typealias Codable = Encodable & Decodable
    
    // Adopting Codable on your own types enables you to serialize them to and from any of the built-in data formats, and any formats provided by custom encoders and decoders.
    
    // If you want to encode or decode any model or your custom type, you must conform it to Codable. Few built in types like String, Int, Double, Date and Data are already
    // conform to Codable. Built-in types such as Array, Dictionary, and Optional also conform to Codable whenever they contain Codable types.
    
    // All the properties that can be encoded or decoded as they are either standard Codable types or contain Codable types
    class MyClass: NSObject, Codable {
        
        var myString: String = ""
        var myInt: Int = 0
        var myDate: Date = Date()
        var myArray: [String] = [String]()
        var otherDetails: [String: String] = [String: String]()
        var childObjects:[MyClass] = [MyClass]()
        
    }
    
    // MARK: - Encoding and decoding custom model to and from JSON data
    // There are two encoders available to encode your data to required format:
    // - PropertyListEncoder — Encode your type to plist format
    // - JSONEncoder — Encode your type to JSON format

    func encodingDecodingCustomModels() {
        
        struct CarSize:Codable {
            var height: Double
            var lenght: Double
        }
        
        enum CarType: String, Codable {
            case Unknown
            case SUV
            case Sedan
        }
        
        class Car: NSObject, Codable {
            var name: String = ""
            var companyURL: URL? = nil
            var yearOfManufacture: Int = 0
            var isNew: Bool = true
            var otherDetailsData: [String: String]? = nil
            var carType: CarType = .Unknown
            var carSize: CarSize = CarSize(height: 0, lenght: 0)
        }
        
        // Encoding and decoding custom model to and from JSON data
        // Creating a Car instance and encoding it to JSON
        let car = Car()
        car.name = "Tesla Model 3"
        car.isNew = true
        car.yearOfManufacture = 2018
        car.companyURL = URL(string: "www.tesla.com")
        car.carType = .Sedan
        car.otherDetailsData = ["xolor": "White", "fuelType": "Electric"]
        car.carSize = CarSize(height: 100, lenght: 200)
        
        // Econde to JSON format
        let encodedObject = try? JSONEncoder().encode(car)
        if let encodedObjectJsonString = String(data: encodedObject!, encoding: .utf8) {
            print(encodedObjectJsonString)
        }
        
        // Decoding a JSON string to a Car object
        let jsonString = """
                {
                    "name": "Tesla Model 3",
                    "isNew": true,
                    "yearOfManufacture": 2018,
                    "companyURL": "www.tesla.com",
                    "carType": "Sedan",
                    "otherDetailsData": {
                        "Color": "White",
                        "FuelType": "Electric"
                    },
                    "carSize": {
                        "height": 100,
                        "lenght": 200
                    }
                }
            """
        if let jsonData = jsonString.data(using: .utf8) {
            let carObject = try? JSONDecoder().decode(Car.self, from: jsonData)
            print(carObject?.name ?? "")
        }
    }
    
    
    // MARK: - Choosing specific properties to Encode/Decode (skipping unecesary properties)
    // An API JSON response may incluide a bunch of values that your application many not need. In such case it would be good to
    // decode only the values one needs rather than the whole JSON file.
    
    func specificPropertiesToEncondeAndDecode () {
        // Codable types can declare a special nested enumeration named CodingKeys that conforms to the CodingKey protocol. When this enumeration is present,
        // its cases serve as the authoritative list of properties that must be included when instances of a codable type are encoded or decoded. The names of
        // the enumeration cases should match the names you’ve given to the corresponding properties in your type. Omit properties from the CodingKeys
        // enumeration if they won’t be present when decoding instances, or if certain properties shouldn’t be included in an encoded representation.
        
        // CodingKey has RawType as String and Enum values must match case with the JSON key names.
        enum Gender: String, Codable{
            case Male
            case Female
        }
        
        class Persona: NSObject, Codable {
            var name: String = ""
            var age: Int = 0
            var gender: Gender = .Male
            var phone: String = ""
            var country: String = ""
            
            enum CodingKeys: String, CodingKey {
                // Econding/decoding will only include the properties defined in this enum, the rest will be ignored
                case name
                case age
                case gender
            }
        }
        
        // Econding the Persona class
        let persona = Persona()
        persona.name = "Segata Sanshiro"
        persona.age = 30
        persona.gender = .Male
        persona.phone = "666"
        persona.country = "Nippon"
        let encodedObjectPersona = try? JSONEncoder().encode(persona)
        if let encodedPersonaObjectJsonString = String(data: encodedObjectPersona!, encoding: .utf8) {
            // It will encode only selected properties - name, age, gender as we have defined CodingKeys enum in the Persona class.
            // 'Phone' and 'Country' values will not be included in JSON
            print(encodedPersonaObjectJsonString)
        }
        
        // Decode: Consider the below JSON, conforming to the Person class metadata. Decoded object from JSON will have
        // only name, age, gender properties populated but ‘phone’ and ‘country’ will be nil as we excluded these
        // properties for Encoding/Decoding in CodingKeys enum.
        let jsonString2 = """
                            {   "name": "Sands, Charles",
                                "age": 666,
                                "gender": "Male",
                                "phone": "999999",
                                "country": "Winterfell" }
                          """
        if let jsonData2 = jsonString2.data(using: .utf8) {
            // Only with name, age, gender properties decoded from json as we have defined CodingKeys enum in Person class.
            // person.phone and person.country will be empty
            let personObject = try? JSONDecoder().decode(Persona.self, from: jsonData2)
            print(personObject?.name ?? "")
        }
    }
}
