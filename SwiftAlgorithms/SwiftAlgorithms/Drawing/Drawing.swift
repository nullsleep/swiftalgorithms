//
//  Drawing.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 9/21/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation
import UIKit

class Drawing {
    
    // Reference: https://www.youtube.com/watch?v=G4D_EhPi7Qk
    static func drawCircle() -> UIView {
        // Instantiate an empty view:
        // let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        
        let view = CircleView(frame: CGRect(x: 70, y: 100, width: 240, height: 240))
        view.backgroundColor = .clear
        return view
    }
    
    static func drawPolygon() -> UIView {
        return UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
    }
}
