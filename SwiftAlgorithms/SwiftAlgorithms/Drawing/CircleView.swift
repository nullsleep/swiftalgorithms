//
//  CircleView.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 11/5/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation
import UIKit

// Reference: https://www.youtube.com/watch?v=G4D_EhPi7Qk

class CircleView: UIView {
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.lineWidth = 16.0
        UIColor.white.setStroke() // Stroke subsequent views with a white color
        
        // Drawing a line
        //path.addLine(to: CGPoint(x: 100, y: 100))
        
        // x^2 + y^2 = r^2
        // cos(Θ) = x / r ==> x = r * cos(Θ)
        // sin(Θ) = y / r ==> y = r * sin(Θ)
        
        let radius: Double = Double(rect.width) / 2 - 20
        let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
        
        path.move(to: CGPoint(x: center.x + CGFloat(radius), y: center.y))
        
        // Note: For some reason iOS dwaws 360 with a hole in it. So we make It is 361 instead of 360 to avoid such whole.
        // by: determines the number of triangles that is going to draw. The lower the number the more trinagles it draws.
        for i in stride(from: 0, to: 361.0, by: 1) {
            
            // radians = degrees * PI / 180
            let radians = i * Double.pi / 180
            let x = Double(center.x) + radius * cos(radians)
            let y = Double(center.y) + radius * sin(radians)
            
            path.addLine(to: CGPoint(x: x, y: y))
            print(x, y)
        }
        path.stroke()
    }
}
