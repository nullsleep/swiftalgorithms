//
//  DrawingPolygons.swift
//  SwiftAlgorithms
//
//  Created by Carlos Arenas on 11/5/18.
//  Copyright © 2018 Polygon. All rights reserved.
//

import Foundation
import UIKit

class PolygonView: UIView {
    
    override func draw(_ rect: CGRect) {
        let shape = CAShapeLayer()
        shape.opacity = 0.5
        shape.lineWidth = 2
        shape.lineJoin = CAShapeLayerLineJoin.miter
        shape.strokeColor = UIColor.blue.cgColor
        shape.fillColor = UIColor.purple.cgColor
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 120, y: 20))
        path.addLine(to: CGPoint(x: 120, y: 20))
        path.addLine(to: CGPoint())
    }
}
